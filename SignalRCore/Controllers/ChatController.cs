﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Services.Chat;

namespace SignalRCore.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class ChatController : Controller
    {
        private readonly IChatService _chatService;

        /// <summary>
		/// Chat service controller default constructor
		/// </summary>
		/// <param name="chatService">Chat messaging service</param>
		public ChatController(IChatService chatService)
        {
            _chatService = chatService ?? throw new ArgumentNullException(nameof(chatService));
        }

        /// <summary>
		/// Get all chat messages
		/// </summary>
		/// <returns></returns>
		[HttpGet]
        public async Task<IEnumerable<ChatMessageDTO>> Get() => (await _chatService.Get()).OrderByDescending(r => r.ReceviedUtc);

        /// <summary>
		/// Post new chat message
		/// </summary>
		/// <param name="chatMessage">Chat message text</param>
		/// <returns></returns>
		[HttpPost]
        public async Task<ChatMessageDTO> Post([FromBody] ChatMessageDTO chatMessage) =>
            await _chatService.Send(chatMessage.Message);
    }
}
