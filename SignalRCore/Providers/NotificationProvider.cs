﻿using Microsoft.AspNetCore.SignalR;
using Services.Chat;
using SignalRCore.Hubs;
using System;
using System.Threading.Tasks;

namespace SignalRCore.Providers
{
    public class NotificationProvider : INotificationProvider
    {
        private readonly IHubContext<ChatHub> _context;

        public NotificationProvider(IHubContext<ChatHub> context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public async Task Push(ChatMessageDTO chatMessage) =>
            await _context.Clients.All.InvokeAsync("Send", chatMessage);
    }
}
