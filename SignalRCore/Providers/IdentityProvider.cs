﻿using Microsoft.AspNetCore.Http;
using Services.Authentication;
using Services.User;
using System;
using System.Security.Claims;

namespace SignalRCore.Providers
{
    public class IdentityProvider : IIdentityProvider
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        public IdentityProvider(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor
                ?? throw new ArgumentNullException(nameof(httpContextAccessor));
        }

        public AppUserDTO Get()
        {
            var user = _httpContextAccessor.HttpContext.User;
            var userIdentity = user.Identity;

            if (!userIdentity.IsAuthenticated)
                throw new UnauthorizedAccessException();

            var userName = user.FindFirst(ClaimTypes.NameIdentifier).Value;
            return new AppUserDTO(userName);
        }
    }
}
