import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { TokenInterceptor } from './authentication/token.interceptor';
import { JwtInterceptor } from './authentication/jwt.interceptor';
import { HttpClientModule, HttpClient } from '@angular/common/http';

import { AppComponent } from './app.component';
import { NavMenuComponent } from './shared/navmenu/navmenu.component';
import { HomeComponent } from './home/home.component';
import { FetchDataComponent } from './samples/fetchdata/fetchdata.component';
import { CounterComponent } from './samples/counter/counter.component';
import { AuthService } from './authentication/services/auth.service';
import { AuthGuard } from './authentication/auth.guard';
import { ConfigService } from './shared/utils/config.service';
import { AccountModule } from './account/account.module';
import { MessageModule } from './message/message.module';

@NgModule({
    declarations: [
        AppComponent,
        NavMenuComponent,
        CounterComponent,
        FetchDataComponent,
        HomeComponent
    ],
    imports: [
        CommonModule,
        HttpModule,
        FormsModule,
        AccountModule,
        MessageModule,
        HttpClientModule,
        RouterModule.forRoot([
            { path: '', redirectTo: 'home', pathMatch: 'full' },
            { path: 'home', component: HomeComponent },
            { path: 'counter', component: CounterComponent },
            { path: 'fetch-data', component: FetchDataComponent, canActivate: [AuthGuard] },
            { path: '**', redirectTo: 'home' }
        ])
    ],
    providers: [
        {
            provide: HTTP_INTERCEPTORS,
            useClass: TokenInterceptor,
            multi: true
        },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: JwtInterceptor,
            multi: true
        },
        AuthService,
        AuthGuard,
        ConfigService
    ]
})
export class AppModuleShared {
}
