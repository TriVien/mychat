import { Component } from '@angular/core';
import { AuthGuard } from '../authentication/auth.guard';

function getWindow(): any {
    const hasWindow =
        typeof window === 'object' && window !== null && window.self === window;

    return hasWindow ? window : {};
}

@Component({
    selector: 'home',
    templateUrl: './home.component.html',
    providers: [AuthGuard]
})
export class HomeComponent {
    public postList: any;

    constructor() {
        this.postList = getWindow().postList;
    }
}
