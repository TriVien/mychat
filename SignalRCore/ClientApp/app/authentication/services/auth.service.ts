﻿import { Injectable } from '@angular/core';

@Injectable()
export class AuthService {
    private tokenName: string = 'auth_token';

    public getToken(): string | null {
        if (typeof localStorage === 'undefined') {
            return null;
        }
        return localStorage.getItem(this.tokenName);
    }

    public setToken(auth_token: any): void {
        if (typeof localStorage === 'undefined') {
            return;
        }
        localStorage.setItem(this.tokenName, auth_token)
    }

    public removeToken(): void {
        if (typeof localStorage === 'undefined') {
            return;
        }
        localStorage.removeItem(this.tokenName);
    }

    public isAuthenticated(): boolean {
        // get the token
        const token = this.getToken();

        return !!token;
    }
}