﻿import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '../shared/modules/shared.module';

import { UserService } from '../shared/services/user.service';

import { routing } from './message.routing';
import { MessageListComponent } from './message-list/message-list.component';


@NgModule({
    imports: [
        CommonModule, FormsModule, routing, SharedModule
    ],
    declarations: [MessageListComponent],
    providers: [UserService]
})
export class MessageModule { }
