﻿export class ChatMessageModel {
    Message: string = "New message";
    User: AppUserModel;
}

export class AppUserModel {
    Name: string = "John Doe";
}