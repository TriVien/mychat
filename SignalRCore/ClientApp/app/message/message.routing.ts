﻿import { ModuleWithProviders } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MessageListComponent } from './message-list/message-list.component';
import { AuthGuard } from '../authentication/auth.guard';

export const routing: ModuleWithProviders = RouterModule.forChild([
    { path: 'message', component: MessageListComponent, canActivate: [AuthGuard] },
]);