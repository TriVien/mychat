﻿import { Component, Inject } from '@angular/core';
import { AuthGuard } from '../../authentication/auth.guard';
import { ChatMessageModel } from '../models/chat-message.model';
import { Http, Headers, RequestOptions } from '@angular/http';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { HubConnection, TransportType } from "@aspnet/signalr-client";
import { ConfigService } from '../../shared/utils/config.service';
import { AuthService } from '../../authentication/services/auth.service';
import { Subject } from 'rxjs';
import { Observable } from 'rxjs/Observable';

@Component({
    selector: 'app-message-list',
    templateUrl: './message-list.component.html',
    styleUrls: ['./message-list.component.css'],
    providers: [AuthGuard]
})
export class MessageListComponent {
    public currentMessage: string;

    public chatMessages: ChatMessageModel[] = [];

    private chatHub: HubConnection;

    private http: HttpClient;

    private originUrl: string;

    private chatApiUrl: string;

    public connected: boolean = false;

    get messageCount() {
        return this.chatMessages.length;
    }

    constructor(http: HttpClient, private configService: ConfigService, private authService: AuthService) {
        this.http = http;
        this.originUrl = configService.getBaseURI();
        let token = authService.getToken();
        this.chatApiUrl = this.originUrl + "/api/chat";

        this.chatHub = new HubConnection(
            this.originUrl + "/chathub?token=" + token,
            { transport: TransportType.WebSockets | TransportType.LongPolling });

        this.chatHub.on(
            "Send",
            data => this.receiveMessage(data));

        this.chatHub
            .start()
            .catch(error => console.log(error));

        this.connected = true;
    }

    ngOnInit() {
        this.http.get(this.chatApiUrl)
            .subscribe(data => {
                this.chatMessages = data as ChatMessageModel[];
            }, error => {
                console.log(error);
            });
    }

    ngOnDestroy() {
        this.chatHub.stop();
    }

    private receiveMessage(data: any) {
        let chatMessage = data as ChatMessageModel;
        this.chatMessages.unshift(chatMessage);
    }

    sendMessage() {
        if (this.connected == false) {
            alert("Please, wait...");

            return;
        }

        if (this.currentMessage == "") {
            alert("Please, enter your message");

            return;
        }

        let newMessage = new ChatMessageModel();

        newMessage.Message = this.currentMessage;

        var url = this.chatApiUrl;

        let header = new HttpHeaders({ 'Content-Type': 'application/json' });
        let options = { headers: header };

        this.http.post(url, newMessage, options)
            .subscribe(data => {
                this.currentMessage = "";
            }, error => {
                console.log(error);
            });
    }
}
