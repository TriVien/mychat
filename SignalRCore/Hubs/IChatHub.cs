﻿using System.Threading.Tasks;
using Services.Chat;

namespace SignalRCore.Hubs
{
    public interface IChatHub
    {
        Task Send(ChatMessageDTO chatMessage);
    }
}
