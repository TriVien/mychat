﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;
using Services.Chat;
using System;
using System.Threading.Tasks;

namespace SignalRCore.Hubs
{
    [Authorize]
    public sealed class ChatHub : Hub<IChatHub>
    {
        public async Task Send(ChatMessageDTO chatMessage)
        {
            if (chatMessage == null)
                throw new ArgumentNullException(nameof(chatMessage));

            await Clients.All.Send(chatMessage);
        }
    }
}
