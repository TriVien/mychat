﻿using AutoMapper;
using DataAccess.Models;
using Services.Chat;
using Services.User;

namespace SignalRCore.ModelMappings
{
    public class EntityToDTOMappingProfile : Profile
    {
        public EntityToDTOMappingProfile()
        {
            CreateMap<ChatMessage, ChatMessageDTO>();
            CreateMap<AppUser, AppUserDTO>();
        }
    }
}
