﻿using AutoMapper;
using DataAccess.Models;
using Services.Chat;
using Services.User;

namespace SignalRCore.ModelMappings
{
    public class EntityFromDTOMappingProfile : Profile
    {
        public EntityFromDTOMappingProfile()
        {
            CreateMap<ChatMessageDTO, ChatMessage>();
            CreateMap<AppUserDTO, AppUser>();
        }
    }
}
