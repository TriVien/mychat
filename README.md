# README #

### What is this repository for? ###

* Sample Chat using ASP.NET Core 2.0, Authorized SignalR Hub and Angular 4

### How do I get set up? ###

* Ensure the _baseURI defined in SignalRCore\ClientApp\app\shared\utils\config.service.ts matches the launchUrl of the project.
* In appsettings.json, correct the ConnectionStrings, i.e. "DefaultConnection": "Server=.;Database=SignalRCore;Trusted_Connection=True;MultipleActiveResultSets=true" 
* In Package Manager Console, execute Update-Database command to restore the database from models.
* Use DataAccess\TestData\DefaultUsers.sql to create sample users, the default password for sample users is '123456'
* Launch the app and play around!