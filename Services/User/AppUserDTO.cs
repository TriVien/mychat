﻿namespace Services.User
{
    public class AppUserDTO
    {
        public AppUserDTO(string name)
        {
            Name = name;
        }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Name { get; set; }
    }
}
