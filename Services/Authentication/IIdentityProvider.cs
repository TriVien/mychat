﻿using Services.User;

namespace Services.Authentication
{
    public interface IIdentityProvider
    {
        AppUserDTO Get();
    }
}
