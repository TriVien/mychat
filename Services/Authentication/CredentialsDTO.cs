﻿using FluentValidation.Attributes;

namespace Services.Authentication
{
    [Validator(typeof(CredentialsDTOValidator))]
    public class CredentialsDTO
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}
