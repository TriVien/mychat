﻿using System.Threading.Tasks;

namespace Services.Chat
{
    public interface INotificationProvider
    {
        Task Push(ChatMessageDTO chatMessage);
    }
}
