﻿using FluentValidation.Attributes;
using Services.User;
using System;
using System.ComponentModel.DataAnnotations;

namespace Services.Chat
{
    [Validator(typeof(ChatMessageDTOValidator))]
    public class ChatMessageDTO
    {
        public Guid Id { get; set; }
        public string Message { get; set; }
        public DateTime ReceviedUtc { get; set; }
        public AppUserDTO User { get; set; }

        public ChatMessageDTO(string message, DateTime receivedUtc, AppUserDTO user)
        {
            Message = message ?? throw new ArgumentNullException(nameof(message));
            ReceviedUtc = receivedUtc;
            User = user;
        }
    }
}
