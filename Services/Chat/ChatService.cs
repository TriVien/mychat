﻿using Services.Authentication;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Services.Chat
{
    public class ChatService : IChatService
    {
        private readonly IChatMessageStorage _chatMessageStorage;
        private readonly IIdentityProvider _identityProvider;
        private readonly INotificationProvider _notificationProvider;

        public ChatService(IChatMessageStorage chatMessageStorage,
            IIdentityProvider identityProvider,
            INotificationProvider notificationProvider)
        {
            _chatMessageStorage = chatMessageStorage;
            _identityProvider = identityProvider;
            _notificationProvider = notificationProvider;
        }

        public async Task<IEnumerable<ChatMessageDTO>> Get() => await _chatMessageStorage.GetAll();

        public async Task<ChatMessageDTO> Send(string message)
        {
            var currentDateTimeUtc = DateTime.UtcNow;

            var user = _identityProvider.Get();

            var chatMessage = new ChatMessageDTO(message, currentDateTimeUtc, user);

            await _chatMessageStorage.Push(chatMessage);

            await _notificationProvider.Push(chatMessage);

            return chatMessage;
        }
    }
}
