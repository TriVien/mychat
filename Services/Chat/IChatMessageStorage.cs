﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Services.Chat
{
    public interface IChatMessageStorage
    {
        Task<IEnumerable<ChatMessageDTO>> GetAll();

        Task<ChatMessageDTO> Get(Guid messageUid);

        Task Push(ChatMessageDTO chatMessage);

        Task Flush();
    }
}
