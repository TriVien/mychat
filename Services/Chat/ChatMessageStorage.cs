﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Services.Chat
{
    public class ChatMessageStorage : IChatMessageStorage
    {
        private readonly ConcurrentQueue<ChatMessageDTO> _messageQueue = new ConcurrentQueue<ChatMessageDTO>();

        public Task<ChatMessageDTO> Get(Guid messageUid) =>
            Task.Factory.StartNew(() => _messageQueue.SingleOrDefault(_ => _.Id == messageUid));

        public Task<IEnumerable<ChatMessageDTO>> GetAll() =>
            Task.Factory.StartNew(() => _messageQueue.AsEnumerable());

        public async Task Push(ChatMessageDTO chatMessage)
        {
            if (chatMessage == null) throw new ArgumentNullException(nameof(chatMessage));

            chatMessage.Id = Guid.NewGuid();

            _messageQueue.Enqueue(chatMessage);
        }

        public async Task Flush()
        {
            // TODO: Flush all chat messages from in-memory storage to persistent storage (SQL) using Dapper?
        }
    }
}
