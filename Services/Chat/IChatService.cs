﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Services.Chat
{
    public interface IChatService
    {
        Task<IEnumerable<ChatMessageDTO>> Get();
        Task<ChatMessageDTO> Send(string message);
    }
}
