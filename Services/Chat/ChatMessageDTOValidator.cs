﻿using FluentValidation;

namespace Services.Chat
{
    public class ChatMessageDTOValidator : AbstractValidator<ChatMessageDTO>
    {
        public ChatMessageDTOValidator()
        {
            RuleFor(vm => vm.Id).NotEmpty();
            RuleFor(vm => vm.Message).NotNull();
            RuleFor(vm => vm.ReceviedUtc).NotEmpty();
        }
    }
}
