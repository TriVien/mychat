﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DataAccess.Models
{
    public class ChatMessage
    {
		[Required]
        public Guid Id { get; set; }

        [Required]
        public string Message { get; set; }

        [Required]
        public DateTime ReceviedUtc { get; set; }

        public AppUser User { get; set; }

        public ChatMessage(string message, DateTime receivedUtc, AppUser user)
        {
            Message = message ?? throw new ArgumentNullException(nameof(message));
            ReceviedUtc = receivedUtc;
            User = user;
        }
    }
}
